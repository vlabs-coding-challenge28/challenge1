    function addme()
    {
        var row=document.getElementById("row").value;
        var temp="t";
        if(row!=0)
        {
            //table created
            const tbl=document.createElement("table");
            const tblBody=document.createElement("tbody");
            //first row of table
            const row1=document.createElement("tr");
            const cell1=document.createElement("th");
            const cellText1=document.createTextNode("Enter Number");
            cell1.appendChild(cellText1);
            row1.appendChild(cell1);
            const cell2=document.createElement("th");
            const cellText2=document.createTextNode("It's Cube");
            cell2.appendChild(cellText2);
            row1.appendChild(cell2);

            row1.setAttribute("class","heading");
            tblBody.appendChild(row1);
            //others row of table
            for(let i=0;i<row;i++)
            {
                //next row of table
                const row=document.createElement("tr");

                for(let j=0;j<2;j++)
                {
                    const cell=document.createElement("td");
                    cell.setAttribute("class","talign");
                    if(j==0)
                    {
                        const t=document.createElement("input");
                        t.setAttribute("type","text");
                        t.setAttribute("id",i);
                        t.setAttribute("class","tmargin");
                        cell.appendChild(t);
                    }
                    else
                    {
                        cell.setAttribute("id",temp+i);
                }
                    row.appendChild(cell);
                }
                tblBody.appendChild(row);
            }
            tbl.appendChild(tblBody);
            // document.body.appendChild(tbl);
            document.getElementById("demo").innerHTML=tbl.innerHTML;
            const bu="<input type='button' value='Find Cube' class='btn' onclick='showme()'>"
            //document.body.appendChild(bu);
            document.getElementById("sub_btn").innerHTML=bu;
        }
        else
        {
            alert("Please select correct option");
            document.getElementById("demo").innerHTML="";
            document.getElementById("sub_btn").innerHTML="";
        }
    }
    function showme(){
        var row=document.getElementById("row").value;
        var temp="t";
        let flag=0;
        for(let i=0;i<row;i++)
        {
            var ele=document.getElementById(i).value;
            if(ele.length==0)
            {
                flag=1;
            }
            else
            {
                ele=ele*ele*ele;
                document.getElementById(temp+i).innerHTML=ele;
            }
        }
        if(flag==1)
        {
            alert("You have not entered data in few cells");
        }
    }